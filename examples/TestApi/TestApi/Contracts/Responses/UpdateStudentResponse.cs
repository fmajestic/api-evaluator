﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Contracts.Responses
{
    public class UpdateStudentResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<Class> EnrolledClasses { get; set; }

    }
}
