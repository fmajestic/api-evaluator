﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApi.Contracts
{
    public static class ApiRoutes
    {
        public const string Base = "api";


        public static class Students
        {
            public const string GetAll = Base + "/students";

            public const string Update = Base + "/students/{personId}";

            public const string Delete = Base + "/students/{personId}";

            public const string Get = Base + "/students/{personId}";

            public const string Create = Base + "/students";
        }

        public static class Class
        {
            public const string GetAll = Base + "/class";

            public const string Get = Base + "/class/{className}";

            public const string Create = Base + "/class";

            public const string Delete = Base + "/class/{className}";
        }

        public static class Professor
        {

            public const string GetAll = Base + "/professor";

            public const string Update = Base + "/professor/{personId}";

            public const string Delete = Base + "/professor/{personId}";

            public const string Get = Base + "/professor/{personId}";

            public const string Create = Base + "/professor";
        }
    }
}
