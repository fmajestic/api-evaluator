﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Services;
using TestApi.Contracts;

namespace TestApi.Controllers
{
    public class ClassController:Controller
    {
        private readonly IClassService _classService;
        public ClassController(IClassService classService)
        {
            _classService=classService;
            
        }
        [HttpGet(ApiRoutes.Class.GetAll)]
        public IActionResult GetAll()
        {
            return Ok(_classService.GetClasses());
        }

        [HttpGet(ApiRoutes.Class.Get)]
        public IActionResult Get([FromRoute]string className)
        {
            return Ok(_classService.GetClassByName(className));
        }
    }
}
