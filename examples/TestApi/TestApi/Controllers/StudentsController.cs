﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Contracts;
using TestApi.Contracts.Requests;
using TestApi.Contracts.Responses;
using TestApi.Domain;
using TestApi.Services;

namespace TestApi.Controllers
{
    public class StudentsController:Controller
    {
        private readonly IStudentService _studentService;
        public StudentsController(IStudentService studentService)
        {
            _studentService = studentService;
                
        }

        [HttpGet(ApiRoutes.Students.GetAll)]
        public IActionResult GetAll()
        {
           return Ok(_studentService.GetStudents());
        }

        [HttpGet(ApiRoutes.Students.Get)]
        public IActionResult Get([FromRoute]Guid personId)
        {
            return Ok(_studentService.GetStudentByID(personId));
        }

        [HttpPut(ApiRoutes.Students.Update)]
        public IActionResult Update([FromBody]UpdateStudentRequest updateStudentRequest, [FromRoute]Guid personId)
        {
            var classes = new List<Class>(_studentService.GetStudentByID(personId).Classes);
            Class tmp = _studentService.EnrollClass(updateStudentRequest.ClassName);
            classes.Add(tmp);
            if (_studentService.GetStudentByID(personId).Classes.Contains(tmp))
            {
                return BadRequest("Already Enrolled in Class");
            }
            var student = new Student
            {
                Id = personId,
                FirstName = updateStudentRequest.FirstName,
                LastName = updateStudentRequest.LastName,
                Age = updateStudentRequest.Age,
                Address = updateStudentRequest.Address,
                Classes = classes,
                Year = updateStudentRequest.Year
            };
            var succ = _studentService.UpdateStudent(student);
            if (succ)
            {
                var resp = new UpdateStudentResponse { FirstName = student.FirstName, LastName = student.LastName, Id = student.Id,EnrolledClasses= _studentService.GetStudentByID(personId).Classes };
                return Ok(resp);
            }
            else
            {
                return NotFound();
            }


        }
     

        [HttpPost(ApiRoutes.Students.Create)]
        public IActionResult Create([FromBody] CreateStudentRequest studentRequest)
        {
            var student = new Student { Id = Guid.NewGuid(), FirstName = studentRequest.FirstName, LastName = studentRequest.LastName, Age = studentRequest.Age, Address = studentRequest.Address, Year = studentRequest.Year, Classes = studentRequest.Classes };

            if (!_studentService.PostStudent(student))
            {
                return BadRequest("Already Exists");
            }
            var response = new CreateStudentResponse { FirstName = student.FirstName, LastName = student.LastName, Age = student.Age };

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.Students.Get.Replace("{personId}", student.Id.ToString());
            return Created(locationUri,response);
        }
    }
}
