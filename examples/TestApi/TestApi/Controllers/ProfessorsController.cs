﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Contracts;
using TestApi.Services;

namespace TestApi.Controllers
{
    public class ProfessorsController : Controller
    {
        private readonly IProfessorService _professorService;
        public ProfessorsController(IProfessorService professorService)
        {
            _professorService = professorService;
        }
        [HttpGet(ApiRoutes.Professor.GetAll)]
        public IActionResult GetAll()
        {
            return Ok(_professorService.GetProfessors());
        }
        [HttpGet(ApiRoutes.Professor.Get)]
        public IActionResult Get([FromRoute]Guid personId)
        {
            return Ok(_professorService.GetProfessorById(personId));
        }
    }
}
