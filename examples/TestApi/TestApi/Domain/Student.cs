﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApi.Domain
{
    public class Student:Person
    {
        public int Year { get; set; }
        public List<Class> Classes { get; set; }

    }
}
