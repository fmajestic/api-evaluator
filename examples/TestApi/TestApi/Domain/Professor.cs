﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApi.Domain
{
    public class Professor:Person
    {
        public string Institute { get; set; }
        public string Science { get; set; }
        public int Salary { get; set; }
    }
}
