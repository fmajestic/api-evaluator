﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApi.Domain
{
    public class Class
    {
        public String ClassName { get; set; }
        public string Science { get; set; }

        public Professor Professor { get; set; }

        public int ECTS { get; set; }

    }
}
