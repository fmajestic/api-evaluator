﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Services
{
    public class ProfessorService : IProfessorService
    {
        private readonly List<Professor> _professors;
        public ProfessorService()
        {
            _professors = new List<Professor>();
            _professors.Add(new Professor { FirstName = "Neven", LastName = "Elezovic",Science="Math", Salary = 7000, Address = "Tamo negdi 78", Age = 60, Id = Guid.NewGuid(), Institute = "ZPM" });
            _professors.Add(new Professor { FirstName = "Vedran", LastName = "Mornar", Science="Computing",Salary = 7000, Address = "Tamo negdi 28", Age = 55, Id = Guid.NewGuid(), Institute = "ZPR" });
            _professors.Add(new Professor { FirstName = "Sasa", LastName = "Ilijic",Science="Physics", Salary = 7000, Address = "Tamo negdi 5", Age = 52, Id = new Guid("b5369dcd-4ce0-42af-9f1b-738eab081282"), Institute = "ZPF" });
        }

        public Professor GetProfessorById(Guid professorId)
        {
           return _professors.SingleOrDefault(x => x.Id == professorId);
        }

        public Professor GetProfessorByName(string name)
        {
            return _professors.SingleOrDefault(x => x.FirstName.Equals(name));
        }

        public List<Professor> GetProfessors()
        {
            return _professors;
        }
    }
}
