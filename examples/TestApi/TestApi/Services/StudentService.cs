﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Services
{
    public class StudentService : IStudentService
    {
        private readonly List<Student> _students;
        IClassService _classService;
        public StudentService(IClassService classService)
        {
            _students = new List<Student>();
            _classService = classService;
            List<Class> classes = new List<Class>();
            classes.Add(_classService.GetClassByName("PIPI"));
            classes.Add(_classService.GetClassByName("Mat3R"));
            _students.Add(new Student
            {
                FirstName = "Bruno",
                LastName = "Jeric",
                Address = "Poljicka cesta 69",
                Age = 21,
                Id = new Guid("5fb7097c-335c-4d07-b4fd-000004e2d28c"),
            Year = 3,
                Classes = classes
            });
        }

        public Class EnrollClass(string className)
        {
            var ret = _classService.GetClassByName(className);
            if (ret != null)
                return ret;
            else return null;
        }

        public Student GetStudentByID(Guid StudentId)
        {
            return _students.SingleOrDefault(x => x.Id == StudentId);
        }
        public Student GetStudentByName(string fName,string lName)
        {
            return _students.SingleOrDefault(x => x.FirstName == fName && x.LastName==lName);
        }


        public List<Student> GetStudents()
        {
            return _students;
        }

        public bool PostStudent(Student student)
        {
            var exists = GetStudentByName(student.FirstName,student.LastName) != null;
            if (exists) return false;
            if(student.Classes==null)
            {
                student.Classes = new List<Class>();
            }
            student.Classes.Add(_classService.GetClassByName("Mat3R"));
            _students.Add(student);
            return true;
        }

        public bool UpdateStudent(Student student)
        {
            var exists = GetStudentByID(student.Id) != null;
            if (!exists) return false;
            var index = _students.FindIndex(x => x.Id == student.Id);
            _students[index] = student;
            return true;
        }
    }
}
