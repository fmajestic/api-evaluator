﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Services
{
    public interface IClassService
    {
        List<Class> GetClasses();
        Class GetClassByName(String className);
    }
}
