﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Services
{
    public interface IStudentService
    {
        List<Student> GetStudents();
        Student GetStudentByID(Guid StudentId);

        bool UpdateStudent(Student student);

        bool PostStudent(Student student);

        Class EnrollClass(string className);



    }
}
