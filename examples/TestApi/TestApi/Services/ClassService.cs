﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Services
{
    public class ClassService : IClassService
    {
        private readonly List<Class> _classes;
        private readonly IProfessorService _professorService;

        public ClassService(IProfessorService professorService)
        {
            _classes = new List<Class>();
            _professorService = professorService;
            _classes.Add(new Class{
                ClassName = "Mat3R",
                ECTS = 5,
                Science = "Math",
                Professor = professorService.GetProfessorByName("Neven")
            });
            _classes.Add(new Class
            {
                ClassName = "Fiz2",
                ECTS = 5,
                Science = "Physics",
                Professor = professorService.GetProfessorByName("Sasa")
            }); _classes.Add(new Class
            {
                ClassName = "PIPI",
                ECTS = 6,
                Science = "Computing",
                Professor = professorService.GetProfessorByName("Vedran")
            });

        }
        public Class GetClassByName(string className)
        {
            return _classes.SingleOrDefault(x => x.ClassName == className);

        }

        public List<Class> GetClasses()
        {
            return _classes;
        }
    }
}
