﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Domain;

namespace TestApi.Services
{
    public interface IProfessorService
    {
        List<Professor> GetProfessors();
        Professor GetProfessorById(Guid professorId);
        Professor GetProfessorByName(string name);
    }
}
